#include <stdio.h>
#include <stdlib.h>

// Definição da estrutura da árvore binária
typedef struct Arvore {
    int valor;
    struct Arvore *esquerda;
    struct Arvore *direita;
} Arvore;

// Função para criar um novo nó da árvore
Arvore *cria_no(int valor) {
    Arvore *novo_no = (Arvore *)malloc(sizeof(Arvore));
    if (novo_no == NULL) {
        fprintf(stderr, "Erro na alocação de memória.\n");
        exit(1);
    }
    novo_no->valor = valor;
    novo_no->esquerda = NULL;
    novo_no->direita = NULL;
    return novo_no;
}

// Função para verificar se duas árvores são espelhos uma da outra
int eh_espelho(Arvore *arv_a, Arvore *arv_b) {
    // Se ambas as árvores forem vazias, elas são espelhos
    if (arv_a == NULL && arv_b == NULL) {
        return 1;
    }
    // Se uma das árvores for vazia e a outra não, elas não são espelhos
    if (arv_a == NULL || arv_b == NULL) {
        return 0;
    }
    // Verifica se os valores dos nós são iguais e recursivamente verifica as subárvores
    return (arv_a->valor == arv_b->valor) &&
           eh_espelho(arv_a->esquerda, arv_b->direita) &&
           eh_espelho(arv_a->direita, arv_b->esquerda);
}

// Função para criar uma árvore espelho a partir de uma árvore dada
Arvore *cria_espelho(Arvore *arv_a) {
    // Se a árvore for vazia, a árvore espelho também será vazia
    if (arv_a == NULL) {
        return NULL;
    }
    // Cria um novo nó com o mesmo valor
    Arvore *novo_no = cria_no(arv_a->valor);
    // Recursivamente cria as subárvores espelho
    novo_no->esquerda = cria_espelho(arv_a->direita);
    novo_no->direita = cria_espelho(arv_a->esquerda);
    return novo_no;
}

int main() {
    // Exemplo de uso
    Arvore *arvore1 = cria_no(1);
    arvore1->esquerda = cria_no(2);
    arvore1->direita = cria_no(2);

    Arvore *arvore2 = cria_no(1);
    arvore2->esquerda = cria_no(2);
    arvore2->direita = cria_no(2);

    int resultado = eh_espelho(arvore1, arvore2);
    if (resultado) {
        printf("As árvores são espelhos uma da outra.\n");
    } else {
        printf("As árvores não são espelhos uma da outra.\n");
    }

    Arvore *espelho_arvore1 = cria_espelho(arvore1);
    printf("Árvore original:\n");
    // Função para imprimir a árvore (implementação não incluída neste exemplo)
    imprimir_arvore(arvore1);
    printf("\nÁrvore espelho:\n");
    imprimir_arvore(espelho_arvore1);

    // Liberar memória (implementação não incluída neste exemplo)
    liberar_arvore(arvore1);
    liberar_arvore(arvore2);
    liberar_arvore(espelho_arvore1);

    return 0;
}
